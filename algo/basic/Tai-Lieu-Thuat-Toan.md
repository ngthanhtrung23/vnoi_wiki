# Một số Tài liệu hay về Thuật Toán

**1. Giải thuật và lập trình - thầy Lê Minh Hoàng**


Chắc hẳn những bạn học chuyên tin đều biết đến cuốn sách này, bởi nó như là một cuốn sách giáo khoa cho chuyên tin với các kiến thức cơ bản từ quay lui, sắp xếp đến quy hoạch động, đồ thị.

[Link Download](https://drive.google.com/file/d/0BwcTB8a10LBwV1J3T2xDTGhQNmM/view?usp=sharing)

**2. Tài liệu giáo khoa chuyên tin**

Về cơ bản, phần một và phần hai giống với giải thuật và lập trình, tuy nhiên được bổ sung thêm một số kiến thức như: số Catalan, xử lý số lớn bằng xâu,... Kèm theo đó là những bài tập vô cùng hấp dẫn để các bạn luyện tập.

Phần ba chứa các kiến thức nâng cao hơn, đó là hình học và lý thuyết trò chơi.

Link Download:

 - [Tập 1](https://drive.google.com/file/d/0BwcTB8a10LBweWxNcExnVzF5dG8/view?usp=sharing)
 - [Tập 2](https://drive.google.com/file/d/0BwcTB8a10LBwY2kzV0dTMEhYb0E/view?usp=sharing)
 - [Tập 3 - phần 2](https://drive.google.com/file/d/0BwcTB8a10LBwTFBGREdfbnNFYXM/view?usp=sharing)
 - [Tập 3 - phần 2](https://drive.google.com/file/d/0BwcTB8a10LBwdjVOeE10b0tIZ00/view?usp=sharing)

**3. Một số vấn đề đáng chú ý trong môn tin học**

Cuốn sách này được viết bởi các cựu học sinh trường chuyên Phan Bội Châu - Nghệ An. Nó chứa các kiến thức tuyệt hay mà tài liệu giáo khoa chuyên tin hay giải thuật và lập trình đều không có như: duyệt ưu tiên, thuật toán tìm kiếm chuỗi KMP, quy hoạch động trạng thái, quy hoạch động vị trí cấu hình, quy hoạch động trên cây, tô màu đồ thị, thuật toán Dijkstra với đỉnh ảo, Interval Tree, Binary Index Tree, ...

[Link Download](https://drive.google.com/file/d/0BwcTB8a10LBwZHh0c3p0M2NqZ1E/view?usp=sharing)

**4. Bản dịch Introduction to Algorithm**

[Link download](https://drive.google.com/file/d/0B2B4YFwy4LhWWl9NV2pyQno1S0U/view)

(Cảm ơn bạn Nguyễn Văn Khởi đã chia sẻ link).

**5. KC Book**

- [Quyển 1](https://drive.google.com/file/d/0B6O77opfHxDDV1lxdF9jWHVUZDA/view?usp=sharing)
- [Quyển 3](https://drive.google.com/file/d/0BwcTB8a10LBwSmZFdGhSY1lMNVU/view?usp=sharing)